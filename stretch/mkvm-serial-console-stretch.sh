#!/bin/bash
###############################################################################
### QEMU_KVM VM-creation script
##
##  Version:  5.0
##  Released: October 11, 2018
##
##  Notes:
##   Add --print-xml --dry-run to the script command line to generate the
##   VM's XML definition without actually creating the VM.
###############################################################################
set -eu

###############################################################################
### Config Variables
###############################################################################

# VM name.
#   Must be unique among the guest VMs.
#   Must consist of ONLY: letters, numbers, and hyphens
vm="stretch"

# OS variant of the guest VM.
#   Use the command 'osinfo-query os | less -S' to view valid values.
#   Some valid values include: debian9, debiantesting, win7, win10
os_variant="debiantesting"

# Whether to use 'virtio' devices
#   Whether to use KVM virtio devices (for disk, network, console, etc.)
#   Must be one of the following: yes, no
#   Linux guests should set 'yes', for maximum performance.
#   Windows guests should usually set 'no', unless you know what you're doing.
use_virtio="yes"

# Guest console type
#   Must be one of the following: vnc, serial
#   Use 'vnc', unless you really know what you're doing.
console_type="serial"

# VNC port to connect to on the host to view the VM guest's console.
#   Only applicable when console_type="vnc".
#   Every guest with a VNC console must select a unique port on the host.
#   Recommendation: Select a port number in the range 5901 to 5999;
#     convention is to select 59xx, where xx is the last octet of the IP.
#   Alternative recommendation: Set the value to -1, and the VM will,
#     on every startup, dynamically choose a free port on the host.
console_vnc_port=5999

# Host bridge to which to connect the guest VM's NIC.
#   Must be a bridge, not merely a NIC (unlike VirtualBox's bridged adapter).
host_bridge="br0"

# RAM size, in mebibytes; e.g.: 2048 == 2G, 4096 == 4G, 8192 == 8G, etc.
ram_mebibytes=1024

# Storage pool from which to allocate the guest VM's virtual HDD.
storage_pool="vg0"

# Virtual HDD size, in gibibytes
#   This can be a comma-separated list of sizes, to create multiple drives
#   e.g.: 100,1024,1024
hdd_gibibytes=20

# Number of virtual CPUs to give the guest VM.
#   Best to keep this at 1, unless analysis indicates more vcpus would help.
num_cpus=1

# CPU architecture for the VM guest.
#   Must be one of the following: x86_64, i686
guest_arch="x86_64"

# Guest initial boot type, for OS installation.
#   Must be one of the following: cdrom, http
#   Recommendation: Use 'cdrom' for interactive installs,
#   and 'http' for automated installs.
initial_boot="http"

# ISO image to boot from, for guest OS installation.
#   Only applicable when initial_boot="cdrom".
#   Must be a full path name to an ISO file.
boot_iso="$HOME/stretch-mini-amd64.iso"

# HTTP URL to boot from.
#   Only applicable when initial_boot="http".
#   Must be a URL to a web-accessible installer.
#   See the virt-install man page for more info.
boot_http="http://mirror.it.ubc.ca/debian/dists/stretch/main/installer-amd64/"

# Preseed config file, to automate the VM installation.
#   Only applicable when initial_boot="http".
#   Must be either: a full path name to a Debian preseed config file, OR
#   the empty string (to choose no automation).
#   E.g.: "$HOME/preseed-${vm}.cfg"
preseed_cfg="$HOME/vmscripts/preseed-vm-serial-console-stretch.cfg"

###############################################################################
### End of Config Variables
###############################################################################

##
## Function to generate a random MAC address that:
## - is a unicast address (LSB of first octet is 0)
## - is a Locally Administrated Address (2nd LSB of first octet is 1)
## - does not have first octet == 0xfe.
##

random_mac () {
	local -a octets
	while true; do
		octets=( $( hexdump -e '1/1 "%02x" 5/1 " %02x"' -n 6 /dev/urandom ) )
		octets[0]=$( printf "%02x" $[ 0x${octets[0]} & 0xfe | 0x02 ] )
		if [ "fe" != "${octets[0]}" ]; then break; fi
	done
	echo "${octets[*]}" | sed 's/ /:/g'
}

##
## Obtain a temporary work directory, and arrange to remove it on exit.
##

tmpd="$(mktemp -d)"

finish () {
	rm -rf "${tmpd}"
}
trap finish EXIT

##
## Check the VM name.
##

tst=$(echo "$vm" | sed 's/^[a-z0-9][a-z0-9-]*[a-z0-9]$//')
if [ -n "$tst" ]; then
	echo "$vm is not an acceptable VM name."
	exit 1
fi

##
## Check that Windows VMs have console_type="vnc"
##

if [ "${os_variant:0:3}" = "win" ] && [ "${console_type}" != "vnc" ]; then
	echo "Windows guests must set console_type=vnc."
	exit 2
fi

declare -a install_params
install_params=()

location_extra_args=""

##
## Configure disk and network devices.
##

disk_common="pool=${storage_pool},sparse=no,cache=none"
network_common="bridge=${host_bridge},mac=$(random_mac)"

if [ "${use_virtio}" = "no" ]; then

	install_params=(
		"${install_params[@]}"
		--network	"${network_common}"
	)

	for hdd_size in ${hdd_gibibytes//,/ }; do
		install_params=(
			"${install_params[@]}"
			--disk	"${disk_common},size=${hdd_size}"
		)
	done

elif [ "${use_virtio}" = "yes" ]; then

	install_params=(
		"${install_params[@]}"
		--network	"${network_common},model=virtio"
	)

	for hdd_size in ${hdd_gibibytes//,/ }; do
		install_params=(
			"${install_params[@]}"
			--disk	"${disk_common},bus=virtio,size=${hdd_size}"
		)
	done

else
	echo "use_virtio must be one of {yes, no}."
	exit 3
fi

##
## Configure the console.
##

if [ "${console_type}" = "vnc" ]; then

	install_params=(
		"${install_params[@]}"
		--graphics	"vnc,port=${console_vnc_port},listen=127.0.0.1"
	)

elif [ "${console_type}" = "serial" ]; then

	if [ "${use_virtio}" = "no" ]; then

		install_params=(
			"${install_params[@]}"
			--graphics	"none"
			--console	"pty,target_type=serial"
		)

		location_extra_args="${location_extra_args} console=ttyS0,115200n8"

	else
		install_params=(
			"${install_params[@]}"
			--graphics	"none"
			--console	"pty,target_type=serial"
			--console	"pty,target_type=virtio"
		)

		location_extra_args="${location_extra_args} console=hvc0 console=ttyS0,115200n8"
	fi

else
	echo "console_type must be one of {vnc, serial}."
	exit 4
fi

##
## Configure initial boot of the guest, for installation of OS.
##

if [ "${initial_boot}" = "cdrom" ]; then

	install_params=(
		"${install_params[@]}"
		--cdrom		"${boot_iso}"
	)

elif [ "${initial_boot}" = "http" ]; then

	install_params=(
		"${install_params[@]}"
		--location      "${boot_http}"
	)

	if [ -n "${preseed_cfg}" ]; then

		if [ -r "${preseed_cfg}" ]; then

			cp "${preseed_cfg}" "${tmpd}/preseed.cfg"

			install_params=(
				"${install_params[@]}"
				--initrd-inject	"${tmpd}/preseed.cfg"
			)

			location_extra_args="${location_extra_args} auto=true"

		else
			echo "preseed_cfg file does not exist, or it is not readable."
			exit 6
		fi
	fi

	if [ -n "${location_extra_args}" ]; then

		install_params=(
			"${install_params[@]}"
			--extra-args	"${location_extra_args}"
		)
	fi
else
	echo "initial_boot must be one of {cdrom, http}."
	exit 5
fi

##
## Create the VM.
##

install_params=(
	--noautoconsole
	--virt-type	kvm --hvm
	--connect	"${LIBVIRT_DEFAULT_URI:-qemu:///system}"
	--name		"$vm"
	--memory	${ram_mebibytes}
	--vcpus		${num_cpus}
	--cpu		host
	--arch		"${guest_arch}"
	--os-variant	"${os_variant}"
	--features	"acpi=on,apic=on,pae=on"
	"${install_params[@]}"
)

virt-install "${install_params[@]}" "${@}"

##
## Done.
##

exit 0

