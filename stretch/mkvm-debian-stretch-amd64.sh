#!/bin/bash
set -eux

##
## Function to generate a random MAC address that is both:
## - a unicast address (LSB of first octet is 0)
## - a Locally Administrated Address (2nd LSB of first octet is 1)
##

random_mac () {
        local -a octets
        octets=( $( hexdump -e '1/1 "%02x" 5/1 " %02x"' -n 6 /dev/urandom ) )
        octets[0]=$( printf "%02x" $[ 0x${octets[0]} & 0xfe | 0x02 ] )
        echo "${octets[*]}" | sed 's/ /:/g'
}


virt-install \
--connect ${LIBVIRT_DEFAULT_URI:-qemu:///system} \
--name dc1 \
--memory 1024 \
--arch x86_64 \
--cpu host \
--vcpus 2 \
--features acpi=on,apic=on,pae=on \
--pm suspend_to_mem=off,suspend_to_disk=off \
--os-variant debiantesting \
--disk pool=vg0,size=10 \
--network bridge=br0,mac=$(random_mac) \
--graphics vnc \
--noautoconsole \
--hvm \
--virt-type kvm \
--cdrom "$HOME/preseed/stretch-mini-amd64.iso"

